<?php
namespace Hangpu888\Hpadmin;

use think\Model as ThinkModel;

class Model extends ThinkModel
{
    // 设置数据库连接方式
    protected $connection = 'plugin.hangpu888.hpadmin.mysql';
}