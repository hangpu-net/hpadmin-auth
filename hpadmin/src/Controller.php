<?php
namespace Hangpu888\Hpadmin;

class Controller
{
    /**
     * 获取模块名称
     *
     * @return string
     */
    public function getModuleName() : string
    {
        $defaultName = 'admin';
        $referer = request()->header('Referer');
        $parse_url = parse_url($referer);
        if (isset($parse_url['path']) && $parse_url['path']) {
            $pasrse_name = str_replace("/","",$parse_url['path']);
            $modulName = $pasrse_name ? $pasrse_name : $defaultName;
        }else{
            $modulName = $defaultName;
        }
        return $modulName;
    }
}