<?php
namespace Hangpu888\Hpadmin\middleware;

use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;

class CorsMiddleware implements MiddlewareInterface
{
    public function process(Request $request, callable $next) : Response
    {
        if ($request->method() == 'OPTIONS') {
            return response('',204);
        }
        $response = $request->method() == 'OPTIONS' ? response('',204) : $next($request);
        $response->withHeaders([
            'Access-Control-Allow-Origin'           => '*',
            'Access-Control-Allow-Credentials'      => 'true',
            'Content-Type'                          => 'application/json;charset=UTF-8',
            'Access-Control-Allow-Methods'          => 'GET,POST,PUT,DELETE,OPTIONS',
            'Access-Control-Allow-Headers'          => '*'
        ]);
        return $response;
    }
}