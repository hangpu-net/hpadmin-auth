<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class AdsCate extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'tag'               => 'require',
        'width'             => 'require',
        'height'            => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入位置名称',
        'tag.require'       => '请输入位置标签名称',
        'width.require'     => '请输入位置宽度，单位px',
        'height.require'    => '请输入位置高度，单位px',
    ];
    
    protected $scene = [
    ];
}