<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class Ads extends Validate
{
    protected $rule =   [
        'tag'               => 'require',
        'url'               => 'require',
        'sort'              => 'require',
        'imgurl'            => 'require',
    ];
    
    protected $message  =   [
        'tag.require'       => '请选择所属分类',
        'url.require'       => '请输入跳转地址',
        'sort.require'      => '请输入广告排序',
        'imgurl.require'    => '请选择或上传图片',
    ];
    
    protected $scene = [
    ];
}