<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class AdminRole extends Validate
{
    protected $rule =   [
        'title'             => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入职位名称',
    ];
    
    protected $scene = [
    ];    
}