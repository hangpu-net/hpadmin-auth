<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class GroupConfig extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'name'              => 'require',
        'sort'              => 'require',
        'icon'              => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入分组名称',
        'name.require'      => '请输入分组标识',
        'sort.require'      => '请输入分组排序',
        'icon.require'      => '请选择分组图标',
    ];
    
    protected $scene = [
    ];
}