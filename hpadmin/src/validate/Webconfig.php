<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class Webconfig extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'name'              => 'require',
        'type'              => 'require',
        'sort'              => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入配置名称',
        'name.require'      => '请输入标签名称',
        'type.require'      => '请选择表单类型',
        'sort.require'      => '请输入配置排序',
    ];
    
    protected $scene = [
    ];
}