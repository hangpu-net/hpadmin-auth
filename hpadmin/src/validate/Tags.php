<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class Tags extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'tag'               => 'require',
        'view'              => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入标题名称',
        'tag.require'       => '请输入标签名称',
        'view.require'      => '请输入热门指数',
    ];
    
    protected $scene = [
    ];
}