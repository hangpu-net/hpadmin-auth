<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class HelpCate extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'sort'              => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入分类名称',
        'sort.require'      => '请输入分类排序',
    ];
    
    protected $scene = [
    ];
}