<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class AuthRule extends Validate
{
    protected $rule =   [
        'type'              => 'require',
        'title'             => 'require',
        'path'              => 'require',
        'method'            => 'require',
        'sort'              => 'require',
    ];
    
    protected $message  =   [
        'type.require'      => '请选择菜单类型',
        'title.require'     => '请输入菜单名称',
        'path.require'      => '请输入权限路由',
        'method.require'    => '请选择请求类型',
        'sort.require'      => '请输入菜单排序',
    ];
    
    protected $scene = [
    ];    
}