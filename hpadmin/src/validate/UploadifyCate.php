<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class UploadifyCate extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'dir_name'          => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入分类名称',
        'dir_name.require'  => '请输入目录名称',
    ];
    
    protected $scene = [
    ];
}