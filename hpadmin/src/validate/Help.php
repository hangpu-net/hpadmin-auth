<?php
namespace Hangpu888\Hpadmin\validate;

use think\Validate;

class Help extends Validate
{
    protected $rule =   [
        'title'             => 'require',
        'cid'               => 'require',
        'view'              => 'require',
    ];
    
    protected $message  =   [
        'title.require'     => '请输入帮助标题',
        'cid.require'       => '请选择所属分类',
        'view.require'      => '请输入热门指数',
    ];
    
    protected $scene = [
    ];
}