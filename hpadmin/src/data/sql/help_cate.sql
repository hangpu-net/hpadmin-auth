/*
 Navicat Premium Data Transfer

 Source Server         : HPAdminWin开发
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.41.135:3306
 Source Schema         : hpadminwin_com

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/04/2022 21:11:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for php_help_cate
-- ----------------------------
DROP TABLE IF EXISTS `php_help_cate`;
CREATE TABLE `php_help_cate`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ctime` datetime NULL DEFAULT NULL,
  `utime` datetime NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT 100 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '帮助-分类' ROW_FORMAT = DYNAMIC;
SET FOREIGN_KEY_CHECKS = 1;