/*
 Navicat Premium Data Transfer

 Source Server         : HPAdminWin开发
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.41.135:3306
 Source Schema         : hpadminwin_com

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/04/2022 21:12:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for php_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `php_admin_role`;
CREATE TABLE `php_admin_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ctime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `utime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '角色权限规则',
  `is_system` int(11) NULL DEFAULT 0 COMMENT '0不是系统，1是系统',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限-角色' ROW_FORMAT = DYNAMIC;
SET FOREIGN_KEY_CHECKS = 1;
INSERT INTO `php_admin_role` VALUES (1, '2021-11-15 22:27:28', '2022-03-19 17:54:14', '系统管理员', '[\"System\\/group\",\"Console\\/tab\",\"Index\\/welcome\",\"Publics\\/site\",\"Publics\\/login\",\"Index\\/user\",\"Index\\/menu\",\"Index\\/clearMenus\",\"Index\\/getCaches\",\"Uploadify\\/index\",\"Uploadify\\/upload\",\"Uploadify\\/edit\",\"Uploadify\\/move\",\"Uploadify\\/del\",\"UploadifyCate\\/index\",\"UploadifyCate\\/add\",\"UploadifyCate\\/edit\",\"UploadifyCate\\/del\",\"System\\/tab\",\"Config\\/index\",\"Config\\/configlist\",\"Config\\/add\",\"Config\\/del\",\"Config\\/edit\",\"Group\\/index\",\"Group\\/add\",\"Group\\/edit\",\"Group\\/del\",\"Log\\/index\",\"Log\\/info\",\"Auths\\/tab\",\"AuthRule\\/index\",\"AuthRule\\/add\",\"AuthRule\\/edit\",\"AuthRule\\/del\",\"AuthRule\\/getChildren\",\"Role\\/index\",\"Role\\/add\",\"Role\\/edit\",\"Role\\/del\",\"Role\\/auths\",\"Admin\\/index\",\"Admin\\/add\",\"Admin\\/edit\",\"Admin\\/del\",\"Other\\/group\",\"Help\\/tab\",\"HelpCate\\/index\",\"HelpCate\\/add\",\"HelpCate\\/edit\",\"HelpCate\\/del\",\"Help\\/index\",\"Help\\/add\",\"Help\\/edit\",\"Help\\/del\",\"Tags\\/tab\",\"Tags\\/index\",\"Tags\\/add\",\"Tags\\/edit\",\"Tags\\/del\",\"Marketing\\/tab\",\"AdsCate\\/index\",\"AdsCate\\/add\",\"AdsCate\\/edit\",\"AdsCate\\/del\",\"Ads\\/index\",\"Ads\\/add\",\"Ads\\/edit\",\"Ads\\/del\",\"InterfaceApi\\/tab\",\"InterfaceApi\\/index\",\"InterfaceApi\\/add\",\"InterfaceApi\\/edit\",\"InterfaceApi\\/del\",\"InterfaceApi\\/cache\",\"Seller\\/group\",\"Seller\\/tab\",\"Seller\\/index\",\"Seller\\/add\",\"Seller\\/edit\",\"Seller\\/del\",\"Facilities\\/tab\",\"Facilities\\/index\",\"Facilities\\/add\",\"Facilities\\/edit\",\"Facilities\\/del\",\"Goods\\/tab\",\"SellerProduct\\/index\",\"SellerProduct\\/add\",\"SellerProduct\\/edit\",\"SellerProduct\\/del\",\"SellerFinance\\/tab\",\"SellerFinance\\/withdrawal\",\"SellerFinance\\/withdrawalApply\",\"SellerFinance\\/bill\",\"SellerFinance\\/showBillDetail\",\"Game\\/group\",\"Game\\/tab\",\"Game\\/index\",\"Game\\/add\",\"Game\\/edit\",\"Game\\/del\",\"GameArea\\/index\",\"GameArea\\/add\",\"GameArea\\/edit\",\"GameArea\\/del\",\"GameRule\\/index\",\"GameRule\\/add\",\"GameRule\\/edit\",\"GameRule\\/del\",\"roomType\\/tab\",\"GameRoomType\\/index\",\"GameRoomType\\/add\",\"GameRoomType\\/edit\",\"GameRoomType\\/del\",\"GameTeam\\/index\",\"GameTeam\\/add\",\"GameTeam\\/edit\",\"GameTeam\\/del\",\"Match\\/group\",\"GameMatch\\/tab\",\"GameMatch\\/index\",\"Member\\/group\",\"MemberFinance\\/tab\",\"MemberFinance\\/bill\",\"MemberFinance\\/withdrawal\",\"Member\\/tab\",\"Member\\/index\",\"Member\\/edit\",\"Member\\/del\",\"Member\\/showLog\",\"Orders\\/group\",\"Orders\\/tab\",\"Orders\\/matchlist\",\"OrdersGoods\\/tab\",\"Orders\\/goods\"]', 1);