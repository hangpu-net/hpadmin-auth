/*
 Navicat Premium Data Transfer

 Source Server         : HPAdmin2
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.41.135:3306
 Source Schema         : webman2_com

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/04/2022 23:48:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for php_uploadify
-- ----------------------------
DROP TABLE IF EXISTS `php_uploadify`;
CREATE TABLE `php_uploadify`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ctime` datetime NULL DEFAULT NULL COMMENT '上传时间',
  `cid` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名称',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件地址',
  `format` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件格式',
  `size` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件大小',
  `width` int(11) NULL DEFAULT 0 COMMENT '宽度，单位：px（仅图片有效）',
  `height` int(11) NULL DEFAULT 0 COMMENT '高度，单位：px（仅图片有效',
  `adapter` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '选定器：oss阿里云，qiniu七牛云等等',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件-管理器' ROW_FORMAT = DYNAMIC;
SET FOREIGN_KEY_CHECKS = 1;