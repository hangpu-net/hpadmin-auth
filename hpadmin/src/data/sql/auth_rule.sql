/*
 Navicat Premium Data Transfer

 Source Server         : HPAdminWin开发
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.41.135:3306
 Source Schema         : hpadminwin_com

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/04/2022 21:12:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for php_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `php_auth_rule`;
CREATE TABLE `php_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ctime` datetime NULL DEFAULT NULL,
  `utime` datetime NULL DEFAULT NULL,
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求地址：控制器/操作方法',
  `pid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '父级菜单地址',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '附带参数',
  `sort` int(11) NULL DEFAULT 100 COMMENT '排序，值越大越靠后',
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'GET' COMMENT '请求类型：GET，POST，DELETE，PUT，多个使用小写逗号隔开',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'form' COMMENT '类型：form表单页，list表格页，component自定义组件，router纯接口，group分组',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图标类名',
  `show` int(11) NULL DEFAULT 1 COMMENT '是否显示：0隐藏，1显示（仅针对1-2-3级菜单）',
  `client` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'admin' COMMENT '多端：admin默认，根据客户端配置项控制',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 84 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限-规则' ROW_FORMAT = DYNAMIC;
SET FOREIGN_KEY_CHECKS = 1;
INSERT INTO `php_auth_rule` VALUES (1, '2021-11-09 17:30:17', '2021-11-14 03:43:17', 'System/group', '', '系统', '', 100, 'GET', 'group', 'el-icon-setting', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (2, '2021-11-09 17:30:17', '2021-11-13 22:48:46', 'Console/tab', 'System/group', '控制面板', '', 100, 'GET', 'group', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (3, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'System/tab', 'System/group', '系统管理', '', 100, 'GET', 'group', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (4, '2021-11-09 17:30:17', '2021-12-03 11:37:54', 'Auths/tab', 'System/group', '权限管理', '', 100, 'GET', 'group', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (5, '2021-11-09 17:30:17', '2021-11-23 14:11:45', 'Index/welcome', 'Console/tab', '数据分析', 'resources/remote/welcome', 100, 'GET', 'remote', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (6, '2021-11-09 17:30:17', '2021-11-22 17:03:08', 'Config/index', 'System/tab', '系统配置', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (7, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Group/index', 'System/tab', '系统分组', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (8, '2021-11-09 17:30:17', '2021-11-15 21:19:38', 'Log/index', 'System/tab', '登录日志', 'type=0', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (9, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'AuthRule/index', 'Auths/tab', '权限菜单', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (10, '2021-11-09 17:30:17', '2021-12-03 11:38:07', 'Role/index', 'Auths/tab', '部门管理', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (11, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Admin/index', 'Auths/tab', '人事管理', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (12, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Publics/site', 'Console/tab', '应用信息', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (13, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Publics/login', 'Console/tab', '系统登录', '', 100, 'POST', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (14, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Index/user', 'Console/tab', '用户信息', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (15, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Index/menu', 'Console/tab', '获取菜单', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (16, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Index/clearMenus', 'Console/tab', '清除菜单', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (17, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Index/getCaches', 'Console/tab', '清除缓存路由', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (18, '2021-11-09 17:30:17', '2021-11-15 21:19:45', 'Log/index', 'System/tab', '操作日志', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (19, '2021-11-09 17:30:17', '2021-11-14 02:12:53', 'Group/add', 'Group/index', '添加分组', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (20, '2021-11-09 17:30:17', '2021-11-14 02:13:25', 'Group/edit', 'Group/index', '修改分组', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (21, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'Group/del', 'Group/index', '删除分组', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (22, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'AuthRule/add', 'AuthRule/index', '添加权限', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (23, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'AuthRule/edit', 'AuthRule/index', '修改权限', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (24, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'AuthRule/del', 'AuthRule/index', '删除权限', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (25, '2021-11-09 17:30:17', '2021-11-09 17:30:17', 'AuthRule/getChildren', 'AuthRule/index', '获取子权限', '', 100, 'GET', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (28, '2021-11-14 02:14:44', '2021-11-19 17:38:20', 'Uploadify/upload', 'Uploadify/index', '上传附件', '', 100, 'POST', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (29, '2021-11-14 02:16:55', '2021-11-19 17:55:04', 'UploadifyCate/add', 'UploadifyCate/index', '添加附件分类', '', 100, 'POST', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (30, '2021-11-14 03:01:38', '2021-11-14 03:01:38', 'Config/configlist', 'Config/index', '配置项列表', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (31, '2021-11-14 03:03:39', '2021-11-19 17:27:32', 'Config/add', 'Config/index', '添加配置项', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (32, '2021-11-14 03:04:19', '2021-11-19 17:08:42', 'Uploadify/index', 'Console/tab', '附件管理', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (33, '2021-11-14 03:04:44', '2021-11-14 03:04:44', 'Config/del', 'Config/index', '删除配置项', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (34, '2021-11-15 10:57:44', '2021-11-15 10:57:44', 'Config/edit', 'Config/index', '修改配置', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (35, '2021-11-15 10:59:16', '2021-11-22 16:52:17', 'UploadifyCate/index', 'Console/tab', '附件分类管理', '', 100, 'GET', 'router', '', 0, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (36, '2021-11-15 23:16:06', '2021-11-15 23:16:06', 'Role/add', 'Role/index', '添加职位', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (37, '2021-11-15 23:16:32', '2021-11-22 09:11:29', 'UploadifyCate/edit', 'UploadifyCate/index', '修改附件分类', '', 100, 'PUT', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (38, '2021-11-15 23:17:28', '2021-11-15 23:17:28', 'Role/edit', 'Role/index', '修改职位', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (39, '2021-11-15 23:18:07', '2021-11-22 09:12:04', 'UploadifyCate/del', 'UploadifyCate/index', '删除附件分类', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (40, '2021-11-15 23:18:29', '2021-11-15 23:18:29', 'Role/del', 'Role/index', '删除职位', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (41, '2021-11-15 23:20:19', '2021-11-15 23:20:19', 'Role/auths', 'Role/index', '职位授权', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (42, '2021-11-15 23:20:49', '2021-11-22 11:47:38', 'Uploadify/edit', 'Uploadify/index', '修改附件', '', 100, 'PUT', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (43, '2021-11-15 23:22:02', '2021-11-19 16:37:15', 'Admin/add', 'Admin/index', '添加管理员', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (44, '2021-11-15 23:22:26', '2021-11-22 11:48:55', 'Uploadify/move', 'Uploadify/index', '移动附件', '', 100, 'PUT', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (45, '2021-11-15 23:22:52', '2021-11-19 16:37:27', 'Admin/edit', 'Admin/index', '修改管理员', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (46, '2021-11-15 23:23:20', '2021-11-22 11:49:20', 'Uploadify/del', 'Uploadify/index', '删除附件', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (47, '2021-11-15 23:23:43', '2021-11-15 23:23:43', 'Admin/del', 'Admin/index', '删除管理员', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (48, '2021-11-17 14:16:22', '2021-11-17 14:16:22', 'Log/info', 'Log/index', '查看日志详情', '', 100, 'GET', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (60, '2022-01-18 23:58:12', '2022-01-18 23:58:12', 'Other/group', '', '其他', '', 100, 'GET', 'group', 'iconfont hp-inspection', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (61, '2022-01-19 00:00:27', '2022-01-19 00:48:30', 'Help/tab', 'Other/group', '帮助管理', '', 100, 'GET', 'group', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (62, '2022-01-19 00:30:52', '2022-01-19 00:30:52', 'HelpCate/index', 'Help/tab', '帮助分类', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (63, '2022-01-19 00:48:48', '2022-01-19 00:50:11', 'Help/index', 'Help/tab', '系统帮助', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (64, '2022-01-19 00:49:14', '2022-01-19 00:49:14', 'Tags/tab', 'Other/group', '标签管理', '', 100, 'GET', 'group', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (65, '2022-01-19 00:49:38', '2022-01-19 00:49:38', 'Tags/index', 'Tags/tab', '标签单页', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (66, '2022-01-19 00:51:23', '2022-01-19 00:51:23', 'Marketing/tab', 'Other/group', '营销管理', '', 100, 'GET', 'group', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (67, '2022-01-19 00:51:44', '2022-01-19 00:51:44', 'AdsCate/index', 'Marketing/tab', '广告位置', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (68, '2022-01-19 00:51:59', '2022-01-19 00:51:59', 'Ads/index', 'Marketing/tab', '广告列表', '', 100, 'GET', 'list', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (69, '2022-01-20 15:48:17', '2022-01-20 16:17:02', 'HelpCate/add', 'HelpCate/index', '添加帮助分类', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (70, '2022-01-20 15:48:45', '2022-01-20 16:17:09', 'HelpCate/edit', 'HelpCate/index', '编辑帮助分类', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (71, '2022-01-20 15:49:15', '2022-01-20 16:17:14', 'HelpCate/del', 'HelpCate/index', '删除帮助分类', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (72, '2022-01-20 16:18:23', '2022-01-20 16:18:23', 'Help/add', 'Help/index', '添加帮助', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (73, '2022-01-20 16:18:53', '2022-01-20 16:18:53', 'Help/edit', 'Help/index', '修改帮助', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (74, '2022-01-20 16:19:17', '2022-01-20 16:19:17', 'Help/del', 'Help/index', '删除帮助', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (75, '2022-01-20 16:21:02', '2022-01-20 16:22:51', 'Tags/add', 'Tags/index', '添加单页', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (76, '2022-01-20 16:21:40', '2022-01-20 16:23:03', 'Tags/edit', 'Tags/index', '修改单页', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (77, '2022-01-20 16:22:09', '2022-01-20 16:22:09', 'Tags/del', 'Tags/index', '删除单页', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (78, '2022-01-20 16:25:07', '2022-01-20 16:25:07', 'AdsCate/add', 'AdsCate/index', '添加广告位置', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (79, '2022-01-20 16:25:32', '2022-01-20 16:25:32', 'AdsCate/edit', 'AdsCate/index', '修改广告位置', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (80, '2022-01-20 16:25:55', '2022-01-20 16:25:55', 'AdsCate/del', 'AdsCate/index', '删除广告位置', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (81, '2022-01-20 16:26:28', '2022-01-20 16:26:28', 'Ads/add', 'Ads/index', '添加广告', '', 100, 'GET,POST', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (82, '2022-01-20 16:26:52', '2022-01-20 16:26:52', 'Ads/edit', 'Ads/index', '修改广告', '', 100, 'GET,PUT', 'form', '', 1, 'hpadmin');
INSERT INTO `php_auth_rule` VALUES (83, '2022-01-20 16:27:11', '2022-01-20 16:27:11', 'Ads/del', 'Ads/index', '删除广告', '', 100, 'DELETE', 'router', '', 1, 'hpadmin');