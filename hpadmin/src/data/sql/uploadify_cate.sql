/*
 Navicat Premium Data Transfer

 Source Server         : HPAdminWin开发
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.41.135:3306
 Source Schema         : hpadminwin_com

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/04/2022 21:11:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for php_uploadify_cate
-- ----------------------------
DROP TABLE IF EXISTS `php_uploadify_cate`;
CREATE TABLE `php_uploadify_cate`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ctime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `utime` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `dir_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类目录',
  `pid` int(11) NULL DEFAULT 0 COMMENT '父级ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件-分类' ROW_FORMAT = DYNAMIC;
SET FOREIGN_KEY_CHECKS = 1;
INSERT INTO `php_uploadify_cate` VALUES (1, '2022-01-18 15:41:01', '2022-01-18 15:41:01', '后台附件', 'background', 0);