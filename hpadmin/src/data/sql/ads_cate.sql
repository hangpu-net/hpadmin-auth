/*
 Navicat Premium Data Transfer

 Source Server         : HPAdminWin开发
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 192.168.41.135:3306
 Source Schema         : hpadminwin_com

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/04/2022 21:11:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for php_ads_cate
-- ----------------------------
DROP TABLE IF EXISTS `php_ads_cate`;
CREATE TABLE `php_ads_cate`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ctime` datetime NULL DEFAULT NULL,
  `utime` datetime NULL DEFAULT NULL,
  `width` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告宽度（默认不带单位）',
  `height` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告高度（默认不带单位）',
  `tag` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告标签（根据此标签获取广告数据）',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '广告-位置' ROW_FORMAT = DYNAMIC;
SET FOREIGN_KEY_CHECKS = 1;