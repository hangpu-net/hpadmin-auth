APP_DEBUG = true

[APP]
DEFAULT_TIMEZONE = Asia/Shanghai

[DATABASE]
TYPE = {type}
HOSTNAME = {host}
DATABASE = {database}
USERNAME = {username}
PASSWORD = {password}
HOSTPORT = {port}
CHARSET = {charset}
PREFIX = {prefix}