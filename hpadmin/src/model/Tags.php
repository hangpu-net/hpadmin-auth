<?php
namespace Hangpu888\Hpadmin\model;

use Hangpu888\Hpadmin\Model;

class Tags extends Model
{
    // 自动时间戳
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'utime';
}
