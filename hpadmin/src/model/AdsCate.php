<?php
namespace Hangpu888\Hpadmin\model;

use Hangpu888\Hpadmin\Model;

class AdsCate extends Model
{
    // 自动时间戳
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'utime';

    /**
     * 获取宽高尺寸
     *
     * @param [type] $value
     * @param [type] $data
     * @return void
     */
    public function getWidthHeightAttr($value,$data)
    {
        return "宽:{$data['width']} - 高:{$data['height']}";
    }
}
