<?php
namespace Hangpu888\Hpadmin\model;

use Hangpu888\Hpadmin\Model;
use Hangpu888\Hpadmin\utils\manager\DataManager;

class AuthRule extends Model
{
    // 自动时间戳
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'utime';
    
   /**
     * 获取表单所需要数据
     *
     * @return void
     */
    public function getFormData(): array
    {
        $methods = [
            [
                'label'     => 'GET',
                'value'     => 'GET',
            ],
            [
                'label'     => 'POST',
                'value'     => 'POST',
            ],
            [
                'label'     => 'PUT',
                'value'     => 'PUT',
            ],
            [
                'label'     => 'DELETE',
                'value'     => 'DELETE',
            ],
        ];
        $data['methods']    = $methods;
        // 类型：form表单页，tabForm选项卡表单，list表格页，component自定义组件，router纯接口
        $type = [
            [
                'label'     => '分组类型',
                'value'     => 'group',
            ],
            [
                'label'     => '表单类型',
                'value'     => 'form',
            ],
            [
                'label'     => '表格类型',
                'value'     => 'list',
            ],
            [
                'label'     => '接口类型',
                'value'     => 'router',
            ],
            [
                'label'     => '远程组件',
                'value'     => 'component',
            ],
        ];
        $data['type']       = $type;
        $parentList      = AuthRule::limit(1000)->select()->toArray();
        $parent_level       = DataManager::channelLevel($parentList, '', '', 'path', 'pid');
        $parents            = $this->getChildrenOptions($parent_level);
        $parent             = $parents;
        $data['parent']     = $parent;
        $show = [
            [
                'label'     => '隐藏',
                'value'     => 0,
            ],
            [
                'label'     => '显示',
                'value'     => 1,
            ],
        ];
        $data['show']       = $show;
        return $data;
    }

    /**
     * 递归拼接select数据
     *
     * @param array $data
     * @return array
     */
    private function getChildrenOptions(array $data): array
    {
        $type = [
            'welcome'                       => '首页',
            'group'                         => '分组',
            'form'                          => '表单',
            'list'                          => '表格',
            'router'                        => '接口',
            'remote'                        => '远程',
        ];
        $list = [];
        $i = 0;
        foreach ($data as $value) {
            $type_text                      = $type[$value['type']];
            $title                          = "{$value['title']}-{$type_text}";
            $list[$i]['label']              = $title;
            $list[$i]['value']              = $value['path'];
            if ($value['_data']) {
                $list[$i]['children']       = $this->getChildrenOptions($value['_data']);
            }
            $i++;
        }
        return $list;
    }
}