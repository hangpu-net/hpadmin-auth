<?php
namespace Hangpu888\Hpadmin\model;

use Hangpu888\Hpadmin\Model;

class Webconfig extends Model
{
    // 自动时间戳
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'utime';
    
    /**
     * 获取表单选择类型
     *
     * @return void
     */
    public function getFormTypeOptions()
    {
        return [
            [
                'label'     => '输入框',
                'value'     => 'input',
            ],
            [
                'label'     => '下拉框',
                'value'     => 'select',
            ],
            [
                'label'     => '单选框',
                'value'     => 'radio',
            ],
            [
                'label'     => '开关按钮',
                'value'     => 'switch',
            ],
            [
                'label'     => '多选框',
                'value'     => 'checkbox',
            ],
            [
                'label'     => '多文本框',
                'value'     => 'textarea',
            ],
            [
                'label'     => '日期选择',
                'value'     => 'DatePicker',
            ],
            [
                'label'     => '时间选择',
                'value'     => 'TimePicker',
            ],
            [
                'label'     => '颜色选择',
                'value'     => 'ColorPicker',
            ],
            [
                'label'     => '单文件上传',
                'value'     => 'upload',
            ],
            [
                'label'     => '多文件上传',
                'value'     => 'uploads',
            ],
            [
                'label'     => '单图上传',
                'value'     => 'image',
            ],
            [
                'label'     => '多图上传',
                'value'     => 'images',
            ],
        ];
    }
}