<?php
namespace Hangpu888\Hpadmin\model;

use Hangpu888\Hpadmin\Model;
use Hangpu888\Hpadmin\utils\builder\FormBuilder;
use Hangpu888\Hpadmin\utils\builder\ListBuilder;

class ConfigGroup extends Model
{
    // 自动时间戳
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'utime';
    
    /**
     * 显示表格数据
     *
     * @return ListBuilder
     */
    public function showListView() : ListBuilder
    {
        $builder = new ListBuilder;
        $builder = $builder
        ->addColumn('id', '序号',[
            'width'         => '80'
        ])
        ->addColumn('name', '标识')
        ->addColumn('title', '名称')
        ->addColumn('icon', '图标')
        ->addColumn('sort', '排序',[
            'width'         => '80'
        ]);
        return $builder;
    }

    /**
     * 显示表单数据
     *
     * @return FormBuilder
     */
    public function showFormView() : FormBuilder
    {
        $builder = new FormBuilder;
        $builder = $builder
        ->addRow('title','input','名称','',[
            'col'               => [
                'span'          => 12
            ]
        ])
        ->addRow('name','input','标识','',[
            'col'               => [
                'span'          => 12
            ]
        ])
        ->addRow('sort','input','排序','100',[
            'col'               => [
                'span'          => 12
            ]
        ])
        ->addRow('icon','custom','图标','',[
            'type'              => 'icons',
            'extra'             => [
                'col'           => [
                    'span'      => 12
                ]
            ]
        ]);
        return $builder;
    }
}