<?php
namespace Hangpu888\Hpadmin\model;

use Hangpu888\Hpadmin\Model;

class Uploadify extends Model
{
    // 自动时间戳
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'ctime';
    protected $updateTime = 'utime';
    
    // 一对一（查询对应分类）
    public function category()
    {
        return $this->hasOne(UploadifyCate::class, 'id', 'cid');
    }
}
