<?php
return [
    // 公共配置
    'common'                                => [
    ],
    // 筛选构造器配置
    'screen'                                => [
        // 是否开启构造器权限检测
        'auth'                              => false,
    ],
    // 表格配置
    'list'                                  => [
        // 是否开启构造器权限检测
        'auth'                              => true,
    ],
    // 表单配置
    'form'                                  => [
        // 是否开启构造器权限检测
        'auth'                              => false,
        'config'                            => [
            'form'                          => [
                //行内表单模式
                'inline'                    => false,
                //表单域标签的位置，如果值为 left 或者 right 时，则需要设置 label-width
                'labelPosition'             => 'right',
                //表单域标签的后缀
                'labelSuffix'               => '',
                //是否显示必填字段的标签旁边的红色星号
                'hideRequiredAsterisk'      => true,
                //表单域标签的宽度，例如 '50px'。作为 Form 直接子元素的 form-item 会继承该值。支持 auto。
                'labelWidth'                => 'auto',
                //是否显示校验错误信息
                'showMessage'               => true,
                //是否以行内形式展示校验信息
                'inlineMessage'             => false,
                //是否在输入框中显示校验结果反馈图标
                'statusIcon'                => false,
                //是否在 rules 属性改变后立即触发一次验证
                'validateOnRuleChange'      => false,
                //是否禁用该表单内的所有组件。若设置为 true，则表单内组件上的 disabled 属性不再生效
                'disabled'                  => false,
                //用于控制该表单内组件的尺寸 medium / small / mini
                'size'                      => 'small',
                //是否显示 label
                'title'                     => true
            ],
            'row'                           => [
                //栅格间隔
                'gutter'                    => 20,
                //布局模式，可选 flex，现代浏览器下有效
                'type'                      => 'flex',
                //flex 布局下的垂直排列方式 top/middle/bottom
                'align'                     => '',
                //flex 布局下的水平排列方式 start/end/center/space-around/space-between
                'justify'                   => '',
                //自定义元素标签
                'tag'                       => 'div'
            ],
            'submitBtn'                     => [
                //类型 primary / success / warning / danger / info / text
                'type'                      => "primary",
                //尺寸 medium / small / mini
                'size'                      => "",
                //是否朴素按钮
                'plain'                     => false,
                //是否圆角按钮
                'round'                     => false,
                //是否圆形按钮
                'circle'                    => false,
                //是否加载中状态
                'loading'                   => false,
                //是否禁用状态
                'disabled'                  => false,
                //图标类名
                'icon'                      => 'el-icon-upload',
                //按钮宽度
                'width'                     => '10%',
                //是否默认聚焦
                'autofocus'                 => false,
                //原生 type 属性
                'nativeType'                => "button",
                //按钮内容
                'innerText'                 => "保存",
                //按钮是否显示
                'show'                      => true,
            ],
            'resetBtn'                      => [
                'type'                      => "default",
                'size'                      => "",
                'plain'                     => false,
                'round'                     => false,
                'circle'                    => false,
                'loading'                   => false,
                'disabled'                  => false,
                'icon'                      => 'el-icon-refresh',
                'width'                     => '10%',
                'autofocus'                 => false,
                'nativeType'                => "button",
                'innerText'                 => "重置",
                'show'                      => true,
            ],
            'info'                          => [
                //提示消息类型,popover,tooltip
                'type'                      => "popover"
            ]
        ]
    ],
];