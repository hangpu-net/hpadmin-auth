<?php
return [
    'enable'                                => true,
    // 无需登录白名单路由
    'white_route'                           => [
        'hpadmin/Publics/site',
        'hpadmin/Publics/login',
    ],
    // 后台配置
    'background'                            => [
        'admin'                             => [
            // IP138密钥
            'ip138'                     => [
                'key'                   => '',
            ],
            // 地图配置
            'maps'                      => [
                'baidu'                 => [
                    'key'               => '',
                ],
                'amap'                  => [
                    'key'               => '',
                ],
            ],
            // 全局组件配置
            'remote_components'             => [
                // 登录页
                'login'                     => '',
                // 首页顶部工具栏
                'index_top_header'          => '',
                // 首页右侧平台设置
                'index_right_platform'      => '',
                // 首页左侧菜单栏
                'index_left_menus'          => '',
            ],
            // 接口列表
            'api_list'                      => [
                // 常用接口
                'web_api'                   => [
                    // 登录接口
                    'login'                 => 'hpadmin/Publics/login',
                    // 用户信息
                    'user'                  => 'hpadmin/Index/user',
                    // 菜单数据
                    'menu'                  => 'hpadmin/Index/menu',
                    // 缓存项目接口列表获取
                    'caches'                => 'hpadmin/Index/getCaches',
                ],
                // 附件管理器接口
                'uploadify'                 => [
                    // 附件分类
                    'category'              => [
                        'index'             => 'hpadmin/UploadifyCate/index',
                        'add'               => 'hpadmin/UploadifyCate/add',
                        'edit'              => 'hpadmin/UploadifyCate/edit',
                        'del'               => 'hpadmin/UploadifyCate/del',
                    ],
                    // 附件接口
                    'upload'                => [
                        'index'             => 'hpadmin/Uploadify/index',
                        'add'               => 'hpadmin/Uploadify/upload',
                        'edit'              => 'hpadmin/Uploadify/edit',
                        'del'               => 'hpadmin/Uploadify/del',
                        'move'              => 'hpadmin/Uploadify/move',
                    ],
                ],
            ],
            // 动态加载JS跟CSS
            'load_resource'                 => [
                'js'                        => [
                    // 路径可以是本地，也可以是CDN
                    'http://hpadmin1.com/resources/js/default.js',
                ],
                'css'                       => [
                    [
                        'type'              => 'iconfont',
                        'link'              => 'http://hpadmin1.com/resources/css/iconfont.css',
                        'extra'             => 'http://hpadmin1.com/resources/css/iconfont.json',
                    ],
                    [
                        'type'              => 'css',
                        'link'              => 'http://hpadmin1.com/resources/css/default.css',
                    ],
                ],
                'theme'                     => [
                    'active'                => 'default',
                    'list'                  => [
                        'default'           => [
                            'title'         => '默认主题',
                            'style'         => 'http://hpadmin1.com/resources/theme/default/style.css',
                        ],
                        'yellow'            => [
                            'title'         => '黄色主题',
                            'style'         => 'http://hpadmin1.com/resources/theme/yellow/style.css',
                        ],
                    ],
                ],
            ],
        ],
        // 可以复制以上配置建立多后台....
    ],
];
