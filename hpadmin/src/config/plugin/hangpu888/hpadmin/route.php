<?php

use Hangpu888\Hpadmin\Routes;
use Webman\Route;
use support\Request;

// 禁止默认路由
Route::disableDefaultRoute();

// 检测是否已安装
if (file_exists(base_path() . "/.env")) {
    // 应用安装完成
    Route::add(['GET'], '/install/step4.html', function () {
        $viewPath = base_path() . '/vendor/hangpu888/hpadmin/src/view/install/step4.html';
        return response('')->withFile($viewPath);
    });
    Route::add(['POST'], '/step4', '\Hangpu888\Hpadmin\InstallController@step4');

    // 后台访问链接
    $backroundList = config('plugin.hangpu888.hpadmin.app.background');
    foreach ($backroundList as $dirName => $value) {
        // 后台访问地址
        Route::get("/{$dirName}/", function () {
            $viewPath = base_path() . '/vendor/hangpu888/hpadmin/src/view/admin/index.html';
            return response('')->withFile($viewPath);
        });
        // 后台静态资源转发
        Route::get("/{$dirName}/static/[{path:.+}]", function (Request $request, $path = '') {
            $viewPath = base_path() . "/vendor/hangpu888/hpadmin/src/view/admin/static/{$path}";
            return response('')->withFile($viewPath);
        });
    }

    // 默认路由
    Route::get('/', [app\controller\Index::class, 'index']);
    // 注册HPAdmin路由
    $routeClass = new Routes;
    $routeClass->initify();
} else {
    // 跳转安装界面
    Route::get('/', function () {
        return redirect('/install/index.html');
    });
    Route::add(['POST'], '/', '\Hangpu888\Hpadmin\InstallController@index');
    Route::add(['GET', 'POST'], '/step1', '\Hangpu888\Hpadmin\InstallController@step1');
    Route::add(['GET', 'PUT'], '/step2', '\Hangpu888\Hpadmin\InstallController@step2');
    Route::add(['GET', 'POST'], '/step3', '\Hangpu888\Hpadmin\InstallController@step3');
}
// 加载资源
Route::any('/install/[{path:.+}]', function (Request $request, $path = '') {
    // 静态文件目录
    $static_base_path = base_path() . '/vendor/hangpu888/hpadmin/src/view/install';
    // 安全检查，避免url里 /../../../password 这样的非法访问
    if (strpos($path, '..') !== false) {
        return response('<h1>400 Bad Request</h1>', 400);
    }
    // 文件
    $file = "{$static_base_path}/{$path}";
    if (!is_file($file)) {
        return response('<h1>404 Not Found</h1>', 404);
    }
    return response('')->withFile($file);
});
