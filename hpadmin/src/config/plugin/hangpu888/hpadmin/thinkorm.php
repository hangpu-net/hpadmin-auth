<?php
use Hangpu888\Hpadmin\utils\Env;
return [
  'default'                   =>    'mysql',
  'connections'               =>    [
      'mysql'                 =>    [
          // 数据库类型
          'type'              => Env::get('DATABASE.TYPE'),
          // 服务器地址
          'hostname'          => Env::get('DATABASE.HOSTNAME'),
          // 数据库名
          'database'          => Env::get('DATABASE.DATABASE'),
          // 数据库用户名
          'username'          => Env::get('DATABASE.USERNAME'),
          // 数据库密码
          'password'          => Env::get('DATABASE.PASSWORD'),
          // 数据库连接端口
          'hostport'          => Env::get('DATABASE.HOSTPORT'),
          // 数据库连接参数
          'params'            => [],
          // 数据库编码默认采用utf8
          'charset'           => Env::get('DATABASE.CHARSET'),
          // 数据库表前缀
          'prefix'            => Env::get('DATABASE.PREFIX'),
          // 断线重连
          'break_reconnect'   => true,
          // 关闭SQL监听日志
          'trigger_sql'       => false,
      ],
  ],
];