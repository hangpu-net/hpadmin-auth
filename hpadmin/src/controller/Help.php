<?php
declare(strict_types=1);
namespace Hangpu888\Hpadmin\controller;

use Hangpu888\Hpadmin\Controller;
use Hangpu888\Hpadmin\model\Help as ModelHelp;
use Hangpu888\Hpadmin\model\HelpCate;
use Hangpu888\Hpadmin\utils\builder\FormBuilder;
use Hangpu888\Hpadmin\utils\builder\ListBuilder;
use Hangpu888\Hpadmin\utils\Json;
use Hangpu888\Hpadmin\validate\Help as ValidateHelp;

class Help extends Controller
{
    /**
     * 显示列表
     *
     * @return void
     */
    public function index()
    {
        $limit = request()->get('limit',30);
        $page = request()->get('page',1);
        $list = ModelHelp::with(['category'])
        ->order('id desc')
        ->paginate([
            'page'          => $page,
            'list_rows'     => $limit
        ])
        ->each(function($item){
            $item->cate_title = $item->category->title;
            return $item;
        })
        ->toArray();
        $builder = new ListBuilder;
        $data = $builder
            ->addColumn('rightButtonList', '操作', [
                'type'              => 'template',
                'template'          => 'rightButtonList',
                'width'             => '180'
            ])
            ->addTopButton('add', '添加', [
                'title'             => '添加数据',
                'api'               => '/hpadmin/Help/add',
            ])
            ->addRightButton('edit', '修改', [
                'api'               => '/hpadmin/Help/edit',
                'title'             => '修改',
            ], [
                'type'              => 'text'
            ])
            ->addRightButton('delete', '删除', [
                'api'               => '/hpadmin/Help/del',
                'method'            => 'delete',
                'title'             => '温馨提示',
                'content'           => '是否确认删除该数据？',
                'type'              => 'confirm',
            ], [
                'type'              => 'text'
            ])
            ->addColumn('id', '序号', [
                'width'             => '80',
            ])
            ->addColumn('ctime', '添加时间', [
                'width'             => '180',
            ])
            ->addColumn('utime', '更新时间', [
                'width'             => '180',
            ])
            ->addColumn('cate_title', '所属分类')
            ->addColumn('title', '标题名称')
            ->addColumn('view', '热门指数')
            ->setData($list['data'])
            ->setDataPage($list['total'], $list['last_page'], $list['per_page'], $list['current_page'])
            ->create();
        return Json::successRes($data);
    }

    /**
     * 显示添加表单
     *
     * @return void
     */
    public function add()
    {
        try {
            if (request()->method() == 'POST') {
                // 获取数据
                $post = request()->post();
    
                // 数据验证
                hpValidate(new ValidateHelp,$post);
                
                // 数据操作
                $model = new ModelHelp;
                foreach ($post as $field => $value) {
                    $model->$field = $value;
                }
                if ($model->save()) {
                    return Json::success('添加成功');
                } else {
                    return Json::fail('添加失败');
                }
            } else {
                $category = HelpCate::order('id desc')
                ->field('id as value,title as label')
                ->select()
                ->toArray();
                $builder = new FormBuilder;
                $data = $builder
                    ->setMethod('post')
                    ->addRow('title', 'input', '标题名称', '', [
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->addRow('cid', 'select', '所属分类', '', [
                        'options'           => $category,
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->addRow('view', 'input', '热门指数', '0')
                    ->addRow('content', 'custom', '图文内容', '',[
                        'type'              => 'wangeditor'
                    ])
                    ->create();
                return Json::successRes($data);
            }
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 显示编辑表单
     *
     * @return void
     */
    public function edit()
    {
        $id = request()->get('id');
        $model = ModelHelp::where(['id'=> $id])->find();
        try {
            if (request()->method() == 'POST') {
                // 获取数据
                $post = request()->post();
    
                // 数据验证
                hpValidate(new ValidateHelp,$post);
                
                // 数据操作
                foreach ($post as $field => $value) {
                    $model->$field = $value;
                }
                if ($model->save()) {
                    return Json::success('修改成功');
                } else {
                    return Json::fail('修改失败');
                }
            } else {
                if (!$model) {
                    return Json::fail('找不到该数据');
                }
                $info = $model->toArray();
                $category = HelpCate::order('id desc')
                ->field('id as value,title as label')
                ->select()
                ->toArray();
                $builder = new FormBuilder;
                $data = $builder
                    ->setMethod('post')
                    ->addRow('title', 'input', '标题名称', '', [
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->addRow('cid', 'select', '所属分类', '100', [
                        'options'           => $category,
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->addRow('view', 'input', '热门指数', '0')
                    ->addRow('content', 'custom', '图文内容', '',[
                        'type'              => 'wangeditor'
                    ])
                    ->setFormData($info)
                    ->create();
                return Json::successRes($data);
            }
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 删除数据
     *
     * @return void
     */
    public function del()
    {
        $id = request()->get('id');
        $model = ModelHelp::where(['id' => $id])->find();
        if (!$model) {
            return Json::fail('该数据不存在');
        }
        if ($model->delete()) {
            return Json::success('删除成功');
        } else {
            return Json::fail('删除失败');
        }
    }
}
