<?php

declare(strict_types=1);

namespace Hangpu888\Hpadmin\controller;

use Hangpu888\Hpadmin\Controller;
use Hangpu888\Hpadmin\model\AdminLog;
use Hangpu888\Hpadmin\utils\builder\FormBuilder;
use Hangpu888\Hpadmin\utils\builder\ListBuilder;
use Hangpu888\Hpadmin\utils\Json;

class Log extends Controller
{
    /**
     * 日志列表
     *
     * @return void
     */
    public function index()
    {
        $type = request()->get('type', '');
        $typeArr = $type === '' ? [1, 2, 3] : [0];
        $limit = request()->get('limit', 30);
        $page = request()->get('page', 1);
        $logModel = new AdminLog;
        $list = $logModel
            ->with(['role', 'admin'])
            ->whereIn('action_type', $typeArr)
            ->order('id desc')
            ->paginate([
                'list_rows'         => $limit,
                'page'              => $page
            ])
            ->toArray();
        $typeText = ['登录', '新增', '修改', '删除'];
        foreach ($list['data'] as $key => $value) {
            $list['data'][$key]['type_text'] = $typeText[$value['action_type']];
            $list['data'][$key]['role_title'] = $value['role']['title'];
            $list['data'][$key]['admin_user'] = $value['admin']['nickname'];
        }
        $builder = new ListBuilder;
        $data = $builder
            ->addColumn('rightButtonList', '操作', [
                'type'              => 'template',
                'template'          => 'rightButtonList',
                'width'             => '100',
            ])
            ->addRightButton('info', '详情', [
                'api'               => '/hpadmin/Log/info',
                'title'             => '查看日志详情',
            ], [
                'type'              => 'text',
            ])
            ->addColumn('id', '序号', [
                'width'             => '80',
            ])
            ->addColumn('ctime', '操作时间', [
                'width'             => '180',
            ])
            ->addColumn('role_title', '职位名称', [
                'width'             => '150',
            ])
            ->addColumn('admin_user', '操作人员', [
                'width'             => '150',
            ])
            ->addColumn('action_ip', '操作IP', [
                'width'             => '130',
            ])
            ->addColumn('city_name', '所在城市', [
                'width'             => '150',
            ])
            ->addColumn('isp_name', '运 营 商', [
                'width'             => '150',
            ])
            ->addColumn('type_text', '操作类型', [
                'width'             => '100',
            ])
            ->addColumn('action_name', '操作事项')
            ->addColumn('path', '操作地址')
            ->setData($list['data'])
            ->setDataPage($list['total'], $list['last_page'], $list['per_page'], $list['current_page'])
            ->create();
        return Json::successRes($data);
    }

    /**
     * 日志详情
     *
     * @return void
     */
    public function info()
    {
        $id = request()->get('id');
        $model = new AdminLog;
        $info = $model->with(['role', 'admin'])->where(['id' => $id])->find();
        if (!$info) {
            return Json::fail('找不到该数据');
        }
        $info = $info->toArray();
        $info['role_title'] = "{$info['role']['title']} [{$info['role']['id']}]";
        $info['admin_title'] = "{$info['admin']['username']} [{$info['admin']['id']}]";
        // 操作类型：0登录，1新增，2修改，3删除
        $type = ['登录', '新增', '修改', '删除'];
        $info['type_text'] = $type[$info['action_type']];

        $builder = new FormBuilder;
        $data = $builder
            ->addDivider('基础信息')
            ->addRow('id', 'custom', '序号:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 8
                    ],
                ]
            ])
            ->addRow('role_title', 'custom', '角色名称:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 8
                    ],
                ]
            ])
            ->addRow('admin_title', 'custom', '管理员名称:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 8
                    ],
                ]
            ])
            ->addRow('action_ip', 'custom', '操作 IP:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 8
                    ],
                ]
            ])
            ->addRow('city_name', 'custom', '所在城市:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 8
                    ],
                ]
            ])
            ->addRow('type_text', 'custom', '操作类型:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 8
                    ],
                ]
            ])
            ->addDivider('操作数据')
            ->addRow('ctime', 'custom', '操作时间:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 12
                    ],
                ]
            ])
            ->addRow('path', 'custom', '操作地址:', '', [
                'type'          => 'info-text',
                'extra'         => [
                    'col'       => [
                        'span'  => 12
                    ],
                ]
            ])
            ->addRow('params', 'custom', '数据参数:', '', [
                'type'          => 'info-text',
            ])
            ->setFormData($info)
            ->create();
        return Json::successRes($data);
    }
}
