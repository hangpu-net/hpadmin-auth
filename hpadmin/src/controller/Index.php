<?php
declare(strict_types=1);
namespace Hangpu888\Hpadmin\controller;

use Hangpu888\Hpadmin\Controller;
use Hangpu888\Hpadmin\Routes;
use Hangpu888\Hpadmin\utils\Json;

class Index extends Controller
{
    /**
     * 获取用户信息
     *
     * @return void
     */
    public function user()
    {
        $user = request()->user;
        return Json::successRes($user->toArray());
    }

    /**
     * 返回菜单权限
     *
     * @return void
     */
    public function menu()
    {
        try {
            $routeCls = new Routes;
            $response = $routeCls->getAuthRules();
            $menus                  = $response['menus'];
            $routes                 = $response['routes'];
            $rootIndex              = 0;
            $default                = $routes[$rootIndex]['path'];
            $data = [
                'default'           => $default,
                'routes'            => $routes,
                'menus'             => $menus,
            ];
            return Json::successRes($data);
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 获取缓存请求路由
     *
     * @return void
     */
    public function getCaches()
    {
        $data = [
            [
                'title'         => '获取管理员数据',
                'name'          => 'getUserInfo',
                'router'        => 'hpadmin/Index/user',
            ],
            [
                'title'         => '清除左侧菜单',
                'name'          => 'cacheMenu',
                'router'        => 'hpadmin/Index/clearMenus',
            ],
            [
                'title'         => '获取权限节点',
                'name'          => 'getMenu',
                'router'        => 'hpadmin/Index/menu',
            ],
            [
                'title'         => '系统应用配置',
                'name'          => 'site',
                'router'        => 'hpadmin/Publics/site',
            ],
        ];
        return Json::successRes($data);
    }

    /**
     * 清除缓存菜单
     *
     * @return void
     */
    public function clearMenus(){}
}