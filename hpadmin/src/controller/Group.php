<?php
declare(strict_types=1);
namespace Hangpu888\Hpadmin\controller;

use Hangpu888\Hpadmin\Controller;
use Hangpu888\Hpadmin\model\ConfigGroup;
use Hangpu888\Hpadmin\model\Webconfig;
use Hangpu888\Hpadmin\utils\builder\FormBuilder;
use Hangpu888\Hpadmin\utils\builder\ListBuilder;
use Hangpu888\Hpadmin\utils\Json;
use Hangpu888\Hpadmin\validate\GroupConfig;

class Group extends Controller
{
    /**
     * 显示列表
     *
     * @return void
     */
    public function index()
    {
        $list = ConfigGroup::order('id asc')
        ->select()
        ->toArray();
        $builder = new ListBuilder;
        $data = $builder
            ->addColumn('rightButtonList', '操作', [
                'type'              => 'template',
                'template'          => 'rightButtonList',
                'width'             => '230'
            ])
            ->addTopButton('add', '添加', [
                'api'               => '/hpadmin/Group/add',
                'title'             => '添加分组',
                'method'            => 'get',
                'type'              => 'modal',
            ])
            ->addRightButton('config', '配置', [
                'api'               => '/hpadmin/Config/configlist',
                'title'             => '配置项管理',
                'method'            => 'get',
                'type'              => 'table',
            ], [
                'type'              => 'text',
            ])
            ->addRightButton('edit', '编辑', [
                'api'               => '/hpadmin/Group/edit',
                'title'             => '修改分组',
                'method'            => 'get',
                'type'              => 'modal',
            ], [
                'type'              => 'text',
            ])
            ->addRightButton('del', '删除', [
                'api'               => '/hpadmin/Group/del',
                'method'            => 'delete',
                'title'             => '温馨提示',
                'content'           => '是否确认删除该数据？',
                'type'              => 'confirm',
            ], [
                'type'              => 'text',
            ], [
                'type'              => 'warning',
            ])
            ->addColumn('id', '序号', [
                'width'         => '80'
            ])
            ->addColumn('name', '标识')
            ->addColumn('title', '名称')
            ->addColumn('icon', '图标')
            ->addColumn('sort', '排序', [
                'width'         => '80'
            ])
            ->setData($list)
            ->create();
        return Json::successRes($data);
    }

    /**
     * 显示添加表单
     *
     * @return void
     */
    public function add()
    {
        try {
            if (request()->method() == 'POST') {
                // 数据获取
                $post = request()->post();
    
                // 数据验证
                hpValidate(new GroupConfig,$post);
    
                // 数据入库
                $groupModel = new ConfigGroup;
                foreach ($post as $field => $value) {
                    $groupModel->$field = $value;
                }
                if ($groupModel->save()) {
                    return Json::success('添加成功');
                } else {
                    return Json::fail('添加失败');
                }
            } else {
                $builder = new FormBuilder;
                $data = $builder
                    ->setMethod('post')
                    ->addRow('title', 'input', '名称', '', [
                        'col'               => [
                            'span'          => 12
                        ]
                    ])
                    ->addRow('name', 'input', '标识', '', [
                        'col'               => [
                            'span'          => 12
                        ]
                    ])
                    ->addRow('sort', 'input', '排序', '100', [
                        'col'               => [
                            'span'          => 12
                        ]
                    ])
                    ->addRow('icon', 'custom', '图标', '', [
                        'type'              => 'icons',
                        'extra'             => [
                            'col'           => [
                                'span'      => 12
                            ]
                        ]
                    ])
                    ->create();
                return Json::successRes($data);
            }
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 显示编辑表单
     *
     * @return void
     */
    public function edit()
    {
        try {
            $id = request()->input('id');
            if (request()->method() == 'PUT') {
                // 数据获取
                $post = request()->post();
    
                // 数据验证
                hpValidate(new GroupConfig,$post);
    
                // 数据入库
                $groupModel = ConfigGroup::where(['id'=> $id])->find();
                if (!$groupModel) {
                    return Json::fail('该数据不存在');
                }
                foreach ($post as $field => $value) {
                    $groupModel->$field = $value;
                }
                if ($groupModel->save()) {
                    return Json::success('修改成功');
                }else{
                    return Json::fail('修改失败');
                }
            }else{
                $response = ConfigGroup::where(['id'=> $id])->find()->toArray();
                $builder = new FormBuilder;
                $data = $builder
                    ->setMethod('put')
                    ->addRow('title', 'input', '名称', '', [
                        'col'               => [
                            'span'          => 12
                        ]
                    ])
                    ->addRow('name', 'input', '标识', '', [
                        'col'               => [
                            'span'          => 12
                        ]
                    ])
                    ->addRow('sort', 'input', '排序', '100', [
                        'col'               => [
                            'span'          => 12
                        ]
                    ])
                    ->addRow('icon', 'custom', '图标', '', [
                        'type'              => 'icons',
                        'extra'             => [
                            'col'           => [
                                'span'      => 12
                            ]
                        ]
                    ])
                    ->setFormData($response)
                    ->create();
                return Json::successRes($data);
            }
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 删除数据
     *
     * @return void
     */
    public function del()
    {
        $id = request()->get('id');
        $groupModel = ConfigGroup::where(['id'=> $id])->find();
        if (!$groupModel) {
            return Json::fail('该数据不存在');
        }
        $config = Webconfig::where(['cid'=> $groupModel->id])->find();
        if ($config) {
            return Json::fail('该分组下有配置项');
        }
        if ($groupModel->delete())
        {
            return Json::success('删除成功');
        }else{
            return Json::fail('删除失败');
        }
    }
}
