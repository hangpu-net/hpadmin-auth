<?php
declare(strict_types=1);
namespace Hangpu888\Hpadmin\controller;

use Hangpu888\Hpadmin\Controller;
use Hangpu888\Hpadmin\model\Help;
use Hangpu888\Hpadmin\model\HelpCate as ModelHelpCate;
use Hangpu888\Hpadmin\utils\builder\FormBuilder;
use Hangpu888\Hpadmin\utils\builder\ListBuilder;
use Hangpu888\Hpadmin\utils\Json;
use Hangpu888\Hpadmin\validate\HelpCate as ValidateHelpCate;

class HelpCate extends Controller
{
    /**
     * 显示列表
     *
     * @return void
     */
    public function index()
    {
        $limit = request()->get('limit',30);
        $page = request()->get('page',1);
        $list = ModelHelpCate::order('id desc')->paginate([
            'page'          => $page,
            'list_rows'     => $limit
        ])->toArray();
        $builder = new ListBuilder;
        $data = $builder
            ->addColumn('rightButtonList', '操作', [
                'type'              => 'template',
                'template'          => 'rightButtonList',
                'width'             => '180'
            ])
            ->addTopButton('add', '添加', [
                'title'             => '添加数据',
                'api'               => '/hpadmin/HelpCate/add',
            ])
            ->addRightButton('edit', '修改', [
                'api'               => '/hpadmin/HelpCate/edit',
                'title'             => '修改',
            ], [
                'type'              => 'text'
            ])
            ->addRightButton('delete', '删除', [
                'api'               => '/hpadmin/HelpCate/del',
                'method'            => 'delete',
                'title'             => '温馨提示',
                'content'           => '是否确认删除该数据？',
                'type'              => 'confirm',
            ], [
                'type'              => 'text'
            ])
            ->addColumn('id', '序号', [
                'width'             => '80',
            ])
            ->addColumn('ctime', '添加时间', [
                'width'             => '180',
            ])
            ->addColumn('utime', '更新时间', [
                'width'             => '180',
            ])
            ->addColumn('title', '分类名称')
            ->addColumn('sort', '分类排序')
            ->setData($list['data'])
            ->setDataPage($list['total'], $list['last_page'], $list['per_page'], $list['current_page'])
            ->create();
        return Json::successRes($data);
    }

    /**
     * 显示添加表单
     *
     * @return void
     */
    public function add()
    {
        try {
            if (request()->method() == 'POST') {
                // 获取数据
                $post = request()->post();
    
                // 数据验证
                hpValidate(new ValidateHelpCate,$post);
                
                // 数据操作
                $model = new ModelHelpCate;
                foreach ($post as $field => $value) {
                    $model->$field = $value;
                }
                if ($model->save()) {
                    return Json::success('添加成功');
                } else {
                    return Json::fail('添加失败');
                }
            } else {
                $builder = new FormBuilder;
                $data = $builder
                    ->setMethod('post')
                    ->addRow('title', 'input', '分类名称', '', [
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->addRow('sort', 'input', '分类排序', '100', [
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->create();
                return Json::successRes($data);
            }
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 显示编辑表单
     *
     * @return void
     */
    public function edit()
    {
        $id = request()->get('id');
        $model = ModelHelpCate::where(['id'=> $id])->find();
        try {
            if (request()->method() == 'POST') {
                // 获取数据
                $post = request()->post();
    
                // 数据验证
                hpValidate(new ValidateHelpCate,$post);
                
                // 数据操作
                foreach ($post as $field => $value) {
                    $model->$field = $value;
                }
                if ($model->save()) {
                    return Json::success('修改成功');
                } else {
                    return Json::fail('修改失败');
                }
            } else {
                if (!$model) {
                    return Json::fail('找不到该数据');
                }
                $info = $model->toArray();
                $builder = new FormBuilder;
                $data = $builder
                    ->setMethod('post')
                    ->addRow('title', 'input', '分类名称', '', [
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->addRow('sort', 'input', '分类排序', '100', [
                        'col'               => [
                            'span'          => 12
                        ],
                    ])
                    ->setFormData($info)
                    ->create();
                return Json::successRes($data);
            }
        } catch (\Throwable $e) {
            return Json::fail($e->getMessage());
        }
    }

    /**
     * 删除数据
     *
     * @return void
     */
    public function del()
    {
        $id = request()->get('id');
        $model = ModelHelpCate::where(['id' => $id])->find();
        if (!$model) {
            return Json::fail('该数据不存在');
        }
        $helpModel = Help::where(['cid'=> $id])->find();
        if ($helpModel) {
            return Json::fail('该分类下有数据，无法删除');
        }
        if ($model->delete()) {
            return Json::success('删除成功');
        } else {
            return Json::fail('删除失败');
        }
    }
}
