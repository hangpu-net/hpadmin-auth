<?php
namespace Hangpu888\Hpadmin\utils\manager;

use Hangpu888\Hpadmin\model\Admin;
use Exception;

/**
 * @title 权限管理器
 * @desc 控制器描述
 * @author 楚羽幽 <admin@hangpu.net>
 */
class AuthManager
{
    // 权限数据
    private $admin_id;
    private $is_system;
    // 部分权限规则
    private $rules;
    // 全部权限规则
    private $ruleAll;

    /**
     * 初始化权限管理器
     *
     * @param integer $admin_id 管理员ID
     * @param integer $is_system
     */
    public function __construct(int $admin_id,int $is_system)
    {
        $this->admin_id = $admin_id;
        $this->is_system = $is_system;
        $admin = Admin::with(['role'])
        ->where(['id'=> $this->admin_id])
        ->find();
        if (!$admin) {
            throw new Exception('该管理员未找到');
        }
        $this->rules = $admin->role->rule;
    }
    
    /**
     * 根据管理员ID检测是否有权限
     *
     * @param string $path 控制器名/方法名（区分大小写）
     * @return boolean
     */
    public function check(string $path) : bool
    {
        // 系统级管理员
        if ($this->is_system) {
            return true;
        }
        return in_array($path,$this->rules) ? true : false;
    }

    /**
     * 获取权限规则
     *
     * @return array
     */
    public function getRules() : array
    {
        return $this->rules;
    }
}
