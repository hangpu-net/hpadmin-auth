<?php
namespace Hangpu888\Hpadmin\utils\manager;

class NameManager
{
    /**
     * 替换姓名
     *
     * @param [type] $name
     * @return void
     */
    public static function getMaskName(string $name)
    {
        if (mb_strlen($name) > 2) {
            return self::mb_substr_replace($name, '*', 1, -1);
        } else {
            return self::mb_substr_replace($name, '*', 0, -1);
        }
    }

    /**
     * 替换姓名
     *
     * @param string $string
     * @param string $replacement
     * @param integer $start
     * @param [type] $length
     * @param [type] $encoding
     * @return void
     */
    public static function mb_substr_replace(string $string, string $replacement, int $start, $length = null, $encoding = null)
    {
        if (extension_loaded('mbstring') === true) {
            $string_length = (is_null($encoding) === true) ? mb_strlen($string) : mb_strlen($string, $encoding);

            if ($start < 0) {
                $start = max(0, $string_length + $start);
            } else if ($start > $string_length) {
                $start = $string_length;
            }

            if ($length < 0) {
                $length = max(0, $string_length - $start + $length);
            } else if ((is_null($length) === true) || ($length > $string_length)) {
                $length = $string_length;
            }

            if (($start + $length) > $string_length) {
                $length = $string_length - $start;
            }

            if (is_null($encoding) === true) {
                return mb_substr($string, 0, $start) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length);
            }

            return mb_substr($string, 0, $start, $encoding) . $replacement . mb_substr($string, $start + $length, $string_length - $start - $length, $encoding);
        }

        return (is_null($length) === true) ? substr_replace($string, $replacement, $start) : substr_replace($string, $replacement, $start, $length);
    }
}
