<?php

namespace Hangpu888\Hpadmin\utils\manager;

use Hangpu888\Hpadmin\model\Uploadify;
use Hangpu888\Hpadmin\model\UploadifyCate;
use Exception;
use Shopwwi\WebmanFilesystem\FilesystemFactory;

class UploadsManager
{
    // 适配器
    private $factory;
    // 驱动
    private $drive;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $drive = config('plugin.shopwwi.filesystem.app.default');
        $this->drive = $drive;
        $flysystem = FilesystemFactory::get($drive);
        $this->factory = $flysystem;
    }

    /**
     * 上传文件至云储存
     *
     * @param string $filePath 上传的文件地址
     * @param string $saveName 保存文件名称
     * @param string $dir_name 分类目录名
     * @return boolean
     */
    public function upload(string $filePath, string $saveName, string $dir_name): bool
    {
        //检测文件是否存在
        $exists = Uploadify::where(['path'=> $saveName])->count();
        if ($exists) {
            return true;
        }
        $category = UploadifyCate::where(['dir_name' => $dir_name])->find();
        if (!$category) {
            throw new Exception('找不到分类目录');
        }
        // 拼接数据
        $size = filesize($filePath);
        $fileName = basename($saveName);
        $extension = substr(strrchr($saveName, '.'), 1);
        $stream = fopen($filePath, 'r+');
        // 上传文件
        $this->factory->writeStream($saveName, $stream);
        fclose($stream);

        // 图片尺寸
        $width = 0;
        $height = 0;
        if (in_array($extension,['jpg','jpeg','gif','png'])) {
            $getimagesize = getimagesize($filePath);
            $width = $getimagesize[0];
            $height = $getimagesize[1];
        }
        
        // 记录上传
        $uploadModel            = new Uploadify;
        $uploadModel->cid       = $category->id;
        $uploadModel->format    = $extension;
        $uploadModel->size      = get_size($size);
        $uploadModel->path      = $saveName;
        $uploadModel->title     = $fileName;
        $uploadModel->width     = $width;
        $uploadModel->height    = $height;
        $uploadModel->adapter   = $this->drive;
        if ($uploadModel->save()) {
            return true;
        }
        return false;
    }

    /**
     * 删除云端附件
     *
     * @param string $path
     * @return boolean
     */
    public function delete(string $path) : bool
    {
        $this->factory->delete($path);
        return true;
    }

    /**
     * 替换访问链接问储存地址
     *
     * @param string $url
     * @return string
     */
    public function strReplaceUrl(string $url) : string
    {
        return str_replace($this->getUrl().'/','',$url);
    }

    /**
     * 获取默认驱动
     *
     * @return string
     */
    public function getDrive() : string
    {
        return $this->drive;
    }

    /**
     * 获取上传配置
     *
     * @return array
     */
    public function getDefaultUp() : array
    {
        $storage = config('plugin.shopwwi.filesystem.app.storage');
        if (!isset($storage[$this->drive])) {
            throw new Exception('访问默认驱动错误');
        }
        return $storage[$this->drive];
    }

    /**
     * 获取文件访问域名
     *
     * @return string
     */
    public function getUrl() : string
    {
        $storage = $this->getDefaultUp();
        if (!isset($storage['url'])) {
            throw new Exception('访问链接错误');
        }
        return $storage['url'];
    }
}
