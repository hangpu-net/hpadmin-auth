<?php
namespace Hangpu888\Hpadmin\utils;

use support\Response;

class Json
{
    /**
     * 返回成功无结果集消息
     *
     * @param string $msg
     * @return Response
     */
    public static function success(string $msg): Response
    {
        return self::response($msg, 200, []);
    }

    /**
     * 返回GraceUI上传成功消息
     *
     * @param string $msg
     * @param string $response
     * @return Response
     */
    public static function uploadSuccess(string $msg, string $response): Response
    {
        $json['status']    = 'ok';
        $json['data']       = $response;
        $json['result']     = $msg;
        return json($json);
    }

    /**
     * 返回GraceUI上传错误消息
     *
     * @param string $msg
     * @return Response
     */
    public static function uploadError(string $msg): Response
    {
        $json['status']     = 'error';
        $json['data']       = $msg;
        return json($json);
    }

    /**
     * 返回近结果集
     *
     * @param array $data
     * @return Response
     */
    public static function successRes(array $data): Response
    {
        return self::response('success', 200, $data);
    }

    /**
     * 返回带结果集的成功消息
     *
     * @param string $msg
     * @param array $data
     * @return Response
     */
    public static function successFul(string $msg, array $data): Response
    {
        return self::response($msg, 200, $data);
    }

    /**
     * 返回错误的消息数据
     *
     * @param string $msg
     * @return Response
     */
    public static function fail(string $msg): Response
    {
        return self::failFul($msg, 404);
    }

    /**
     * 仅返回错误消息
     *
     * @param string $msg
     * @param integer $code
     * @return Response
     */
    public static function failFul(string $msg, int $code): Response
    {
        $json['msg']            = $msg;
        $json['code']           = $code;
        return json($json);
    }

    /**
     * 返回JSON数据
     *
     * @param string $msg
     * @param integer $code
     * @param array $data
     * @return Response
     */
    public static function response(string $msg, int $code, array $data): Response
    {
        $json['msg']            = $msg;
        $json['code']           = $code;
        $json['data']           = $data;
        return json($json);
    }
}
