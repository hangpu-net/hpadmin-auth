<?php
namespace Hangpu888\Hpadmin\utils\builder;

use FormBuilder\Factory\Elm;
use FormBuilder\Form;

/**
 * @title 筛选查询构造器
 * @desc 专门用于表格上方筛选搜索查询的构造器
 * @author 楚羽幽 <admin@hangpu.net>
 */
class ScreenBuilder extends Form
{
    // 数据
    private $data;
    private $builder;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->data = [
            'api'               => '',
            'method'            => 'get',
            'findBtn'           => [
                //类型 primary / success / warning / danger / info / text
                'type' => "primary",
                //尺寸 medium / small / mini
                'size' => "small",
                //是否朴素按钮
                'plain' => false,
                //是否圆角按钮
                'round' => false,
                //是否圆形按钮
                'circle' => false,
                //是否加载中状态
                'loading' => false,
                //是否禁用状态
                'disabled' => false,
                //图标类名
                'icon' => '',
                //按钮宽度
                'width' => '100%',
                //是否默认聚焦
                'autofocus' => false,
                //原生 type 属性
                'nativeType' => "button",
                //按钮内容
                'innerText' => "查询",
                //按钮布局规则
                'col' => '',
            ],
            'params'            => [],
            'rule'              => [],
        ];
        $builder = Form::elm('', [], []);
        $builder->setMethod('GET');
        $this->builder = $builder;
    }

    /**
     * 设置筛选请求
     *
     * @param [type] $action
     * @return ScreenBuilder
     */
    public function setAction($action): ScreenBuilder
    {
        $this->builder->setAction($action);
        return $this;
    }

    /**
     * 设置筛选请求类型
     *
     * @param [type] $method
     * @return ScreenBuilder
     */
    public function setMethod($method): ScreenBuilder
    {
        $this->builder->setAction($method);
        return $this;
    }

    /**
     * 设置提交按钮配置
     *
     * @param string $name
     * @param string $value
     * @return ScreenBuilder
     */
    public function setSubmitBtn(string $name, string $value): ScreenBuilder
    {
        $config = $this->data['findBtn'];
        $config[$name] = $value;
        $this->data['findBtn'] = $config;
        return $this;
    }

    /**
     * 设置筛选请求参数
     *
     * @param array $params
     * @return ScreenBuilder
     */
    public function setParams(array $params = []): ScreenBuilder
    {
        $this->data['params'] = $params;
        return $this;
    }

    /**
     * 添加筛选输入框
     *
     * @param string $field
     * @param string $title
     * @param string $value
     * @param array $extra
     * @return ScreenBuilder
     */
    public function addInput(string $field, string $title, $value = '', array $extra = []): ScreenBuilder
    {
        $component = Elm::input($field, $title, $value);
        if ($extra) {
            foreach ($extra as $componentType => $componentTypeValue) {
                $component->$componentType($componentTypeValue);
            }
        }
        $this->builder->append($component);
        return $this;
    }

    /**
     * 添加筛选下拉框
     *
     * @param string $field
     * @param string $title
     * @param string $value
     * @param array $extra
     * @return ScreenBuilder
     */
    public function addSelect(string $field, string $title, $value = '', array $extra = []): ScreenBuilder
    {
        $component = Elm::select($field, $title, $value);
        if ($extra) {
            foreach ($extra as $componentType => $componentTypeValue) {
                $component->$componentType($componentTypeValue);
            }
        }
        $this->builder->append($component);
        return $this;
    }

    /**
     * 添加范围时间组件
     *
     * @param string $field
     * @param string $title
     * @param array $value
     * @param array $extra
     * @return ScreenBuilder
     */
    public function addRange(string $field, string $title, array $value, array $extra = []): ScreenBuilder
    {
        $component = Elm::datePicker($field, $title, $value);
        if ($extra) {
            $props = [
                'type'          => 'datetimerange',
                'format'        => 'yyyy-MM-dd HH:mm:ss',
            ];
            if (isset($extra['props'])) {
                $extra['props'] = array_merge($extra['props'], $props);
            } else {
                $extra['props'] = $props;
            }
            foreach ($extra as $componentType => $componentTypeValue) {
                $component->$componentType($componentTypeValue);
            }
        } else {
            $component->prop('type', 'datetimerange');
            $component->prop('format', 'yyyy-MM-dd HH:mm:ss');
        }
        $this->builder->append($component);
        return $this;
    }

    /**
     * 构造数据
     *
     * @return array
     */
    public function create(): array
    {
        $apiUrl                 = $this->builder->getAction();
        $this->request = request();
        if (!$apiUrl) {
            $apiUrl             = $this->request->path();
        }
        $method                 = $this->builder->getMethod();
        $rule                   = $this->builder->formRule();
        $data                   = $this->data;
        $data['api']            = $apiUrl;
        $data['method']         = $method;
        $data['rule']           = $rule;
        return $data;
    }
}
